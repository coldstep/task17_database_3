#1 - 1. Забезпечити параметризовану вставку нових значень у таблицю Співробітники.


DELIMITER /
CREATE DEFINER = root@localhost
PROCEDURE InsertInEmployee
(
	IN surname VARCHAR(30),
	IN name CHAR(30),
	IN midle_name VARCHAR(30),
	IN identity_number CHAR(10),
	IN passport CHAR(10),
	IN experience DECIMAL(10,1),
	IN birthday DATE,
	IN post VARCHAR(15),
	IN pharmacy_id INT(11)
) 
SQL SECURITY INVOKER

INSERT INTO employee(surname,name,midle_name,identity_number,passport,experience,birthday,post,pharmacy_id)
VALUES (surname,name,midle_name,identity_number,passport,experience,birthday,post,pharmacy_id);
/

#2 - Забезпечити реалізацію зв’язку М:М між таблицями Перелік
#    лікарств та Зона впливу, тобто вставити в стикувальну таблицю
#    відповідну стрічку, при цьому відповідні стрічки мають існувати в
#    основних таблицях.

DELIMITER /
CREATE DEFINER = root@localhost
PROCEDURE InsertInMedicine_Zone
(
	IN	medicine_i INT(11),
    IN	zone_i INT(11)
) 
SQL SECURITY INVOKER
IF( medicine_i IN  (SELECT id FROM pharmacy) AND zone_i IN (SELECT id FROM zone))
THEN 
INSERT INTO medicine_zone VALUES(medicine_i,zone_i);
ELSEIF ( medicine_i NOT IN (SELECT id FROM pharmacy))
THEN
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = "Table ";
ELSEIF ( zone_i NOT IN (SELECT id FROM zone))
THEN
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = "Table ";
END IF;
/

#3 - Використовуючи курсор, забезпечити динамічне створення таблиць
#    з іменами Співробітників у поточній БД, з випадковою кількістю
#    стовпців (від 1 до 9). Імена та тип стовпців довільні.

DELIMITER /
CREATE DEFINER = root@localhost
PROCEDURE CreateTablesFromEmployee()
SQL SECURITY INVOKER
BEGIN
	DECLARE done INT DEFAULT false;
    DECLARE temp_surname,temp_name CHAR(25);
    
	DECLARE temp CURSOR FOR SELECT surname,name FROM employee;
    
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = true;
    
    OPEN temp;
    myLoop: LOOP
		FETCH temp INTO temp_surname ,temp_name ;
        IF done = true THEN LEAVE myLoop;
        END IF;
        SET @temp_query=CONCAT('CREATE TABLE ',temp_surname,temp_name,'( name varchar(45) );');
        PREPARE myquery FROM @temp_query;
        EXECUTE myquery;
        DEALLOCATE PREPARE myquery;
	End LOOP;
	CLOSE temp;
END
/
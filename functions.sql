#1 -  Для таблиці Співробітники написати функцію як буде шукати MIN
#     стовпця Трудовий стаж . Потім зробити вибірку даних (SELECT),
#     використовуючи дану функцію.


DELIMITER /
CREATE FUNCTION getEmployerWithMinExperience()
RETURNS DECIMAL(10,1)
reads sql data
deterministic
BEGIN 
	RETURN (SELECT MIN(experience) FROM employee);
END;
END /

#2 - Написати функцію, яка витягує за ключем між таблицями
#    Співробітники та Аптечна установа об’єднане значення полів Назва та
#    №будинку. Потім зробити вибірку усіх даних (SELECT) з таблиці
#    Співробітники, використовуючи дану функцію.

DELIMITER /
CREATE FUNCTION getPlaceWhereEmployerWork(temp_id INT)
RETURNS VARCHAR(100)
reads sql data
deterministic
BEGIN 
	RETURN 
    (
			SELECT CONCAT(' ',name,building_number) 
            FROM pharmacy 
            WHERE id = temp_id
	);
END;
END /


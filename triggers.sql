#1 Забезпечити цілісність значень для структури БД.
#-1

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CheckCorrectEmployeeInsertion
BEFORE INSERT ON employee FOR EACH ROW
BEGIN
IF (new.post NOT IN (SELECT post FROM post WHERE post = new.post)) 	
THEN 
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = "Table post don't have this post_value ";
ELSEIF (new.pharmacy_id NOT IN (SELECT id FROM pharmacy WHERE id = new.pharmacy_id))
THEN 
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = "Table pharmacy don't have this pharmacy_id";
END IF;
END /

#-2

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CheckUpdateEmployee
BEFORE UPDATE ON employee FOR EACH ROW
BEGIN
IF (new.post NOT IN (SELECT post FROM post WHERE post = new.post)) 	
THEN 
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = "Table post don't have this post_value ";
ELSEIF (new.pharmacy_id NOT IN (SELECT id FROM pharmacy WHERE id = new.pharmacy_id) AND  NOT NULL)
THEN 
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = "Table pharmacy don't have this pharmacy_id";
END IF;
END/


#-3

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CorrectDeleteForPost
BEFORE DELETE ON post FOR EACH ROW
BEGIN
IF (old.post IN (SELECT post FROM employee WHERE post = old.post)) 	
THEN 
	DELETE FROM storedpr_db.employee WHERE employee.post=old.post;
END IF;
END /

#-4

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CorrectDeleteForPharmacy
BEFORE DELETE ON pharmacy FOR EACH ROW
BEGIN
IF (old.id IN (SELECT pharmacy_id FROM employee WHERE pharmacy_id = old.id OR old.id IN (SELECT pharmacy_id FROM pharmacy_medicine))) 	
THEN 
	UPDATE storedpr_db.employee SET pharmacy_id = NULL WHERE pharmacy_id = old.id;
    DELETE FROM pharmacy_medicine WHERE pharmacy_id = old.id;
END IF;
END /

#-5

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CorrectDeleteForStreet
BEFORE DELETE ON street FOR EACH ROW
BEGIN
IF (old.street IN (SELECT street FROM street WHERE street = old.street)) 	
THEN 
	UPDATE pharmacy SET street = NULL WHERE street = old.street;
END IF;
END /

#-6

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CorrrectUpdatePharmacy
BEFORE UPDATE ON pharmacy FOR EACH ROW
BEGIN
IF (new.id NOT IN (SELECT pharmacy_id FROM employee) OR new.id NOT IN (SELECT pharmacy_id FROM pharmacy_medicine)) 	
THEN 
	UPDATE employee SET pharmacy_id = new.id WHERE pharmacy_id = old.id;
    UPDATE pharmacy_medicine SET pharmacy_id = new.id WHERE pharmacy_id = old.id;
END IF;
IF (new.street NOT IN (SELECT street FROM street ))
THEN 
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = "Table street don't have this street";
END IF;
END/

#-7

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CheckCorrectPharmacyInsertion
BEFORE INSERT ON pharmacy FOR EACH ROW
BEGIN
IF (new.street NOT IN (SELECT street FROM street)) 	
THEN 
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = "Table street don't have this value ";
END IF;
END /

#-8

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CorrrectUpdateStreet
BEFORE UPDATE ON street FOR EACH ROW
BEGIN
IF (new.street NOT IN (SELECT street FROM pharmacy)) 	
THEN 
	DELETE FROM pharmacy WHERE street = old.street;
END IF;
END/

#-9

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CorrrectUpdatePharmacy_medicine
BEFORE UPDATE ON pharmacy_medicine FOR EACH ROW
BEGIN
IF (new.pharmacy_id NOT IN (SELECT id FROM pharmacy)) 	
THEN 
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = "Table pharmacy don't have this id";
END IF;
IF(new.medicine_id NOT IN (SELECT id FROM medicine))
THEN 
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = "Table medicine don't have this id";
END IF;
END/

#-10

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CorrrectInsertionForPharmacy_medicine
BEFORE INSERT ON pharmacy_medicine FOR EACH ROW
BEGIN
IF (new.pharmacy_id NOT IN (SELECT id FROM pharmacy)) 	
THEN 
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = "Table pharmacy don't have this id";
END IF;
IF(new.medicine_id NOT IN (SELECT id FROM medicine))
THEN 
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = "Table medicine don't have this id";
END IF;
END/

#-11

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CorrrectUpdateMedicine
AFTER UPDATE ON medicine FOR EACH ROW
BEGIN
IF (new.id NOT IN (SELECT medicine_id FROM pharmacy_medicine) OR new.id NOT IN (SELECT medicine_id FROM medicine_zone)) 	
THEN 
	UPDATE pharmacy_medicine SET medicine_id = new.id WHERE medicine_id = old.id;
    UPDATE medicine_zone SET medicine_id = new.id WHERE medicine_id = old.id;
END IF;
END/


#-12

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CorrectDeleteForMedicine
BEFORE DELETE ON medicine FOR EACH ROW
BEGIN
IF (old.id IN (SELECT medicine_id FROM medicine_zone WHERE medicine_id = old.id OR old.id IN (SELECT medicine_id FROM pharmacy_medicine))) 	
THEN 
	DELETE FROM medicine_zone WHERE medicine_id = old.id;
    DELETE FROM pharmacy_medicine WHERE medicine_id = old.id;
END IF;
END /

#-13

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CorrectInsertionForMedicine_zone
BEFORE INSERT ON medicine_zone FOR EACH ROW
BEGIN
IF (new.medicine_id NOT IN (SELECT id FROM medicine)) 	
THEN 
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = "Table medicine don't have this id";
END IF;
IF(new.zone_id NOT IN (SELECT id FROM zone))
THEN 
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = "Table zone don't have this id";
END IF;
END/

#-14

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CorrectUpdateForMedicine_zone
BEFORE UPDATE ON medicine_zone FOR EACH ROW
BEGIN
IF (new.medicine_id NOT IN (SELECT id FROM medicine)) 	
THEN 
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = "Table medicine don't have this id";
END IF;
IF(new.zone_id NOT IN (SELECT id FROM zone))
THEN 
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = "Table zone don't have this id";
END IF;
END/

#-15

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CorrectDeleteForZone
BEFORE DELETE ON zone FOR EACH ROW
BEGIN
IF (old.id IN (SELECT zone_id FROM medicine_zone WHERE zone_id = old.id)) 	
THEN 
	DELETE FROM medicine_zone WHERE zone_id = old.id;
END IF;
END /

#-16

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CorrectUpdateForZone
AFTER UPDATE ON zone FOR EACH ROW
BEGIN
IF (old.id IN (SELECT zone_id FROM medicine_zone WHERE zone_id = old.id)) 	
THEN 
	UPDATE medicine_zone SET zone_id = new.Id WHERE zone_id = old.id;
END IF;
END /
  
#2 Співробітники→ Ідентифікаційний номер не може закінчувати двома нулями;

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER BeforeInsertIdentity_number
BEFORE INSERT ON employee FOR EACH ROW
BEGIN
IF (new.identity_number LIKE ('%00')) THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'You have two zero number in the end of identity_number';
END IF;
END /
DELIMITER ;

#3 для Перелік лікарств→ Код міністерства забезпечити формат вводу: 2 довільні букви, окрім М і П + '-' + 3 цифри + '-' + 2цифри.

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CheckCorrectInsertionMCode
BEFORE INSERT ON medicine FOR EACH ROW
BEGIN
IF (new.ministry_code NOT RLIKE ('[^МП]{2}-[[:digit:]]{3}-[[:digit:]]{2}$')) 
THEN 
	SIGNAL SQLSTATE '45000' 
    SET MESSAGE_TEXT = 'Incorrect ministry_code convention';
END IF;
END /
DELIMITER ;

#4 Заборонити будь-яку модифікацію даних в таблиці Посада.

DELIMITER /
CREATE DEFINER = root@localhost
TRIGGER CorrrectUpdatePost
BEFORE UPDATE ON post FOR EACH ROW
BEGIN
SIGNAL SQLSTATE '45000' 
SET MESSAGE_TEXT = "You can't update this table !!!";
END/
DELIMITER ;